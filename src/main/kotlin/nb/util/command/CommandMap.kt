package nb.util.command

import nb.util.arguments.Arguments


open class CommandMap(
    val mapIdentifier: String,
    private val commandMap: Map<String, Command>,
    private inline val commandNotFound: Command
) {

    constructor(
        mapIdentifier: String,
        aliases: List<String>,
        commands: List<Command>,
        commandNotFound: Command
    ): this(
        mapIdentifier = mapIdentifier,
        commandMap = aliasesAndCommandsToMap(aliases, commands),
        commandNotFound = commandNotFound
    )

    fun execute(arguments: Arguments) {
        val commandName = arguments[CommandKeys.COMMAND_IDENTIFIER] ?: error("ERROR -- no command name given for CommandMap ($mapIdentifier) with arguments: $arguments")

        val command = commandMap.getOrElse(commandName) { return commandNotFound.executeCommand(arguments) }

        command.executeCommand(arguments)
    }

    operator fun contains(key: String) = commandMap.any { key in it.key }



    @CommandMapDSL class Builder(@property:CommandMapDSL var mapIdentifier: String) {

        private val map: MutableMap<String, Command> = mutableMapOf()

        @CommandMapDSL var commandNotFound: Command = genericCommandNotFound

        @CommandMapDSL fun identifier(newIdentifier: String): CommandMap.Builder {
            mapIdentifier = newIdentifier
            return this
        }

        @CommandMapDSL fun addPair(identifier: String, command: Command): CommandMap.Builder {
            map[identifier] = command
            return this
        }

        @CommandMapDSL inline fun addCommand(
            identifier: String,
            command: Command
        ): CommandMap.Builder {
            return addPair(identifier, command)
        }

        @CommandMapDSL inline fun addCommand(command: Command): CommandMap.Builder {
            return addPair(command.commandIdentifier, command)
        }

        @CommandMapDSL inline infix fun String.to(command: Command): CommandMap.Builder = addPair(this, command)

        @CommandMapDSL inline fun command(
            commandIdentifier: String,
            @BuilderInference block: Command.Builder.() -> Unit
        ): CommandMap.Builder {
            val command = buildCommand(commandIdentifier, block)
            return addPair(commandIdentifier, command)
        }

        @CommandMapDSL inline fun commandNotFound(identifier: String, block: Command.Builder.() -> Unit): CommandMap.Builder {
            commandNotFound = buildCommand("${identifier}_$mapIdentifier", block)
            return this
        }

        fun build(): CommandMap = CommandMap(
            mapIdentifier = mapIdentifier,
            commandMap = map,
            commandNotFound = commandNotFound
        )
    }
}
