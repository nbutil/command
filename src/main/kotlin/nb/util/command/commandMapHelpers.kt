package nb.util.command

@DslMarker annotation class CommandMapDSL

fun aliasesAndCommandsToMap(identifiers: List<String>, commands: List<Command>): Map<String, Command> {
    if (identifiers.size != commands.size) {
        throw Exception("Mismatching identifiers and commands lengths -- identifiers: ${identifiers.size}, commands: ${commands.size}")
    }

    val map = (identifiers zip commands).toMap()
    return map
}

val genericCommandNotFound = buildCommand {
    executeCommand {
        println("command \"${get(CommandKeys.COMMAND_IDENTIFIER)}\" not found")
    }
}

inline fun buildCommandMap(
    identifier: String,
    @BuilderInference block: CommandMap.Builder.() -> Unit
) =
    CommandMap.Builder(identifier).apply(block).build()

fun commandMapOf(
    mapIdentifier: String,
    vararg entries: Pair<String, Command>,
    commandNotFound: Command
): CommandMap = CommandMap(
    mapIdentifier = mapIdentifier,
    aliases = entries.map { it.first },
    commands = entries.map { it.second },
    commandNotFound = commandNotFound
)

