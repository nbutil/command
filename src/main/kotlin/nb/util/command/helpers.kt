package nb.util.command

import nb.util.arguments.ArgumentKey
import nb.util.arguments.Arguments


@DslMarker annotation class CommandDSL

@CommandDSL class CommandContext(@CommandDSL inline val arguments: Arguments) {
    @CommandDSL inline fun <R> get(key: ArgumentKey<R>) = arguments[key]!!
}

@CommandDSL class ExpectOptionalContext {
    internal val expected = mutableListOf<ArgumentKey<*>>()

    internal val optional = Arguments(mutableMapOf())

    @CommandDSL fun <T> expectedArgument(key: ArgumentKey<T>) {
        expected.add(key)
    }

    @CommandDSL fun <T> optionalArgument(key: ArgumentKey<T>, defaultValue: T) {
        optional[key] = defaultValue
    }
}


inline fun buildCommand(
    commandIdentifier: String = "NO_COMMAND_IDENTIFIER",
    @BuilderInference block: Command.Builder.() -> Unit
) =
    Command.Builder(commandIdentifier).apply(block).build()
