package nb.util.command

import nb.util.arguments.ArgumentKey

object CommandKeys {
    @JvmField val COMMAND_IDENTIFIER = ArgumentKey<String>("command_identifier")
    @JvmField val COMMAND_DESCRIPTION = ArgumentKey<String>("command_description")
}
