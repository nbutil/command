package nb.util.command

import nb.util.arguments.ArgumentKey
import nb.util.arguments.Arguments
import nb.util.requirement.*

interface Command: (Arguments) -> Unit {

    val commandIdentifier: String

    fun executeCommand(arguments: Arguments)

    override operator fun invoke(arguments: Arguments) = executeCommand(arguments = arguments)

    fun invoke(vararg arguments: Pair<ArgumentKey<*>, *>) = invoke(arguments = Arguments(arguments = arguments))

    @CommandDSL class Builder(
        @property:CommandDSL var commandIdentifier: String,
        @property:CommandDSL var requirementsImpl: RequirementsSignature = ::RequirementsImpl
    ) {

        val requirements = mutableListOf<Requirement>()

        @CommandDSL lateinit var executeCommand: CommandContext.() -> Unit

        @CommandDSL var arguments: ExpectOptionalContext.() -> Unit = { }

        @CommandDSL var commandImpl: CommandImplSignature = ::CommandImpl

        @CommandDSL inline fun requirements(block: RequirementsContext.() -> Unit): Command.Builder {
            RequirementsContext(requirements).apply(block)
            return this
        }

        @CommandDSL fun identifier(identifier: String): Command.Builder {
            commandIdentifier = identifier
            return this
        }

        @CommandDSL fun executeCommand(block: CommandContext.() -> Unit): Command.Builder {
            executeCommand = block
            return this
        }

        @CommandDSL fun arguments(block: ExpectOptionalContext.() -> Unit): Command.Builder {
            arguments = block
            return this
        }

        @CommandDSL fun commandImpl(block: CommandImplSignature): Command.Builder {
            commandImpl = block
            return this
        }

        fun build(): Command {
            val argContext = ExpectOptionalContext().apply(arguments)
            val impl = commandImpl(
                commandIdentifier,
                argContext.expected,
                argContext.optional,
                requirementsImpl(requirements),
                executeCommand
            )
            return impl
        }
    }
}

typealias CommandImplSignature = (
    String,
    List<ArgumentKey<*>>,
    Arguments,
    Requirements,
    CommandContext.() -> Unit
) -> Command

internal class CommandImpl(
    override val commandIdentifier: String,
    val expectedArguments: List<ArgumentKey<*>>,
    val optionalArguments: Arguments,
    val requirements: Requirements,
    inline val executeCommand: CommandContext.() -> Unit,
): Command {
    override fun executeCommand(arguments: Arguments) {
        val notFoundArguments = mutableListOf<ArgumentKey<*>>()
        for (argument in expectedArguments) {
            if (argument !in arguments) {
                notFoundArguments.add(argument)
            }
        }

        if (notFoundArguments.isNotEmpty()) {
            error("Could not find expected arguments: $notFoundArguments")
        }

        arguments.mergeBy(optionalArguments)

        if (requirements(arguments)) {
            val commandContext = CommandContext(arguments)
            commandContext.executeCommand()
        }
    }
}
