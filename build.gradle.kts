import com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar

plugins {
    java
    kotlin("jvm") version "1.5.10"
    id("com.github.johnrengelman.shadow") version "6.0.0"
}

group = "nb.util"
version = "1.0-0"

repositories {
    mavenCentral()
}

fun DependencyHandlerScope.localImpl(fileName: String, path: String = "lib/") = implementation(files("$path$fileName"))

fun DependencyHandlerScope.localJar(fileName: String, path: String = "lib/") = localImpl("$fileName.jar", path)

dependencies {
    implementation(kotlin("stdlib"))
    localJar("arguments-1.0-0")
    localJar("requirement-1.0-0")
}

val compileKotlin: org.jetbrains.kotlin.gradle.tasks.KotlinCompile by tasks
compileKotlin.kotlinOptions {
    freeCompilerArgs = listOf("-Xinline-classes", "-Xopt-in=kotlin.experimental.ExperimentalTypeInference", "-Xopt-in=kotlin.RequiresOptIn", "-Xjvm-default=all")
    jvmTarget = "11"
}

tasks.withType<ShadowJar> {
    archiveBaseName.set(rootProject.name)
    archiveVersion.set(version)
    minimize()
    dependencies {
        exclude(dependency(".*:.*:.*"))
    }
}
